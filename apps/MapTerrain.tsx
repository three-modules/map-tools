/**
 * Features:
 * - switch terrain mode: minecraft blocks, normal
 * - switch texturing: lut, rock, triplantexshader
 * - switch view: normal (orbit), animated first person character, custom cam (view from your window)
 * - infos: altitude, boussole (north, east, ...), minimap
 * - on the fly terrain generation with LOD
 * - lighting conf: sun simulation 
 * - dropdown banner: collapsed (FPS, title, toggle F10 indication, ) expanded(timeline, map, )
 * - fog to limit view
 * 
 * NTH:
 * - cam animation from current to target location
 * - terrain animation on elevation loading
 * - texture change on height (0=sea, ...)
 * - lake recognition: perfectly flat area
 */

import * as THREE from "three";
import { Canvas, useLoader, useThree } from "@react-three/fiber";
import { TextureLoader, Vector2, Vector3 } from "three";
import { TerrainWrapper, TerrainType, TerrainBlocks } from "../../three-core-modules/apps/Terrains";
import { Tile } from "../Tile";
import { Suspense, useEffect, useState } from "react";
import Quadtree from "@timohausmann/quadtree-js";
import { MapTree } from "../MapTree";
import { TILE_ZOOM_LVL } from "./MapGen";
import { Texture } from "three";
import { GeoProj } from "../GeoToolkit";
import { IgnGeoServiceProvider } from "../GeoServices";
import { DataSample } from "../../three-core-modules/DataSources";

const mapRadius = 100;  // radius around person camera in meter

/**
 * Instanciate a terrain for each tile
 * Terrain elevation updated upon data reception:
 * default sea-level (0m) and upon data reception compute average altitude and update terrain
 * @param param0 
 * @returns 
 */
export const MapTerrain = ({ style, ...props }: any) => {
    const { camera } = useThree();
    const [mapNodes, setMapNodes] = useState([]);
    // const [mapArea, setMapArea] = useState(new MapArea([]))

    useEffect(() => {
        console.log("[MapTerrain] init")
        // camera.position.set(mapArea.center.x, 10, mapArea.center.y);
        const camPos = new Vector3(478820, 140, 6143071);
        camera.position.set(...camPos.toArray());
        let nodes = MapTree.getNodes(MapTree.quadtree, TILE_ZOOM_LVL, { x: camPos.x, y: camPos.z, width: mapRadius, height: mapRadius });
        nodes.filter(node => !node.objects.length).forEach(node => {
            console.log("insert new tile")
            node.insert(new Tile(node));
        });
        setMapNodes(nodes);
    }, [])

    return (<>
        {mapNodes.length && mapNodes.map(node => (<Suspense fallback={null}>
            <TerrainConf tileNode={node} />
        </Suspense>))}
    </>
    );
}

const TerrainConf = ({ tileNode, ...props }: { tileNode: Quadtree }) => {
    // const [config, setConfig] = useState();
    // extract tile
    let tile = tileNode.objects[0] as Tile;

    // Load texture
    let tex = useLoader(TextureLoader, tile.imgRemoteUrl) as Texture;
    // tex = buildTex(tile.imgRemoteUrl, 1)
    let elevation = (x, y) => {
        let val = 8 * (x + y) * 0;
        let blk = tile.quadtree.objects[0] as DataSample;
        val = blk ? blk.dataPoint.y : 120;
        return val;
    }

    const geoDataSource = async (dataPoint: Vector3) => {
        console.log("[DataSample] fetching")
        const lonlat = GeoProj.revertFromMercator([dataPoint.x, dataPoint.z]);
        const elev = await IgnGeoServiceProvider.requestElevation(lonlat);
        dataPoint.y = elev;
        return elev;
    }

    const interpolation = (dataPoint: Vector3) => {
        console.log("[DataSample] interpolating");
        const samples = tile.quadtree.retrieve(tile);
        const filtered = samples.filter((sample: DataSample) => !sample.isInterpolated)
        console.log(`Found ${filtered.length} non interpolated records (total: ${samples.length}) `)
        const interpolated = 130;
        dataPoint.y = interpolated;
        return interpolated;
    }

    if (!tile.quadtree) {
        // Tile sampling or partitionning or segmentation
        tile.quadtree = new Quadtree(tileNode.bounds); // terrain sample grid
        const dataSample = new DataSample(tile.quadtree.bounds);
        dataSample.fetch = geoDataSource
        dataSample.interpolate = interpolation
        tile.quadtree.insert(dataSample);
    }

    const terrainConf: any = {
        resol: 4,
        type: TerrainType.Normal,
        datasource: elevation,
        animated: false,
        bounds: tileNode.bounds,
        // material: mat,
        tex: tex
    };



    useEffect(() => {
        DataSample.updated = false;
    })

    // terrainConf.bounds =  { x: 0, y: 0, width: 256, height: 256 };
    return (<>
        <TerrainWrapper refreshGeom={DataSample.updated} {...terrainConf} />
        <TerrainBlocks blockNode={tile.quadtree} />
    </>)
}