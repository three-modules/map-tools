import Apps from "../../appRouting"
import { MApp } from "./MapGen"
import { MapRender } from "./MapRender"
import { MapWidget } from "./MapWidget"

/**
 * Module Apps here
 */
export const MapUtilsModule = () => {
    return (
        // <Routing>
        <Apps>
            <MApp />
            <MapWidget />
            <MapRender/>
        </Apps>
        // </Routing>
    )
}