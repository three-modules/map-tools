# three-mapgen

Map generation tool

# Progress

- [x] map height sampling

In screenshot below, terrain elevation represented by LUT colors. 

![screenshot](screenshot.png)


- [ ] 3D terrain: voxels

- [ ] 3D terrain visualizarion

## Instructions

### Module ecosystem

Part of a module ecosystem requiring other modules to work

The script `.gitpod.yml` does all deployment steps automatically:

### CORS
To retrieve map data, request are made to external API hosted on different domain than the app.
To allow access to external services, CORS has to be enabled on the app server.

Can be done in different ways.
When using create-react-app, it is possible to configure a simple nginx server to act as proxy and pass request to the app server and add the right header.

Assuming your are hosting the app on local port 3000,
here is a working configuration to put in `nginx.conf`

```
 server {
        listen       80;
        server_name  localhost;
        
        location / {
            proxy_pass http://localhost:3000;

             # CORS for simple requests
            if ($request_method ~* "(GET|POST)") {
                add_header "Access-Control-Allow-Origin"  *;
            }
        }
}
```

This tells nginx to redirect anything on port 80 to the app server on 3000 and add a specific header to allow CORS.
The app can then be directly access from localhost (80 port) instead of localhost:3000.
